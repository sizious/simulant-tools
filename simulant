#!/usr/bin/env python3
import requests
import os
from os.path import splitext, join, exists
import sys
import zipfile
from clint.textui import progress
import argparse
import shutil
import platform
import subprocess
import docker
import logging
import site
import json

from xml.dom.minidom import Document


_STR_LENGTH = 120
_PROJECT_ROOT = None
_DOWNLOAD_ROOT = "https://storage.googleapis.com/staging.simulant-engine.appspot.com"

PLACEHOLDER = "__project_name__"


class UnsupportedPlatformError(ValueError):
    def __init__(self, platform):
        super(UnsupportedPlatformError, self).__init__(
            "Your development platform (%s) is currently unsupported" % platform
        )


def _make_executable(path):
    mode = os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(path, mode)


def _project_path(file_path):
    global _PROJECT_ROOT

    if not _PROJECT_ROOT:
        _PROJECT_ROOT = _find_project_root()

    return os.path.join(_PROJECT_ROOT, file_path)


def _rename_files_and_directories(search, replacement, directory):
    " Recursively replaces 'search' in folder and filenames with 'replacement' "
    for root, subdirs, files in os.walk(directory):
        for subdir in subdirs:
            if PLACEHOLDER in subdir:
                path = join(root, subdir)
                dest = join(root, subdir.replace(PLACEHOLDER, replacement))
                shutil.move(path, dest)

        for file in files:
            if PLACEHOLDER in file:
                path = join(root, file)
                dest = join(root, file.replace(PLACEHOLDER, replacement))
                shutil.move(path, dest)


def _search_replace(search, replacement, directory, extensions=[".cpp", ".h", ".md", ".json", ".txt"]):
    "Recursively search replace text in files that match the extensions"
    for root, subdirs, files in os.walk(directory):
        for file in files:
            if splitext(file)[-1] in extensions:
                path = join(root, file)

                with open(path, "r") as f:
                    s = f.read()

                s = s.replace(search, replacement)

                with open(path, "w") as f:
                    f.write(s)


def _find_project_root():
    SIMULANT_CONFIG = "simulant.json"

    path = os.path.abspath(SIMULANT_CONFIG)
    while not os.path.exists(path):
        new_path = os.path.dirname(path)
        if new_path == path:
            print("Unable to find {0}. Are you in the right directory?".format(
                SIMULANT_CONFIG
            ))

        path = new_path

    return os.path.dirname(path)


def _find_shipped_path(path):
    this_dir = os.path.dirname(__file__)

    final = os.path.join(this_dir, path)
    if os.path.exists(final):
        return os.path.abspath(final)

    # pip installs normally install this file to
    # PREFIX/bin/simulant. So the parent of the parent
    # directory should be the prefix
    guessed_prefix = os.path.dirname(this_dir)

    for prefix in (site.PREFIXES + [guessed_prefix]):
        final = os.path.join(prefix, "share/simulant-tools", path)
        if os.path.exists(final):
            return final

    raise ValueError("Unable to find: %s" % path)


def _download_zip_and_extract(zip_file, target_dir):
    r = requests.get(zip_file, stream=True)
    path = os.path.join(target_dir, zip_file.rsplit("/")[-1])

    with open(path, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(
            r.iter_content(chunk_size=1024),
            expected_size=(total_length/1024) + 1
        ):
            if chunk:
                f.write(chunk)
                f.flush()

    zip_ref = zipfile.ZipFile(path, 'r')
    zip_ref.extractall(target_dir)
    zip_ref.close()

    os.remove(path)


def _download_docker_image(image):

    client = docker.from_env()

    try:
        print(("Checking for Docker image %s" % image).ljust(_STR_LENGTH), end='')
        client.images.get(image)
        print("[ OK ]")
    except docker.errors.ImageNotFound:
        print("[ NOT FOUND ]")
        print(("Downloading Docker image %s" % image).ljust(_STR_LENGTH), end='')
        client.images.pull("kazade/dreamcast-sdk")
        print("[ OK ]")
    except requests.exceptions.ConnectionError:
        print("[ FAILED ]")
        sys.exit(1)


def _check_docker_running():
    try:
        print("Checking Docker is running".ljust(_STR_LENGTH), end='')
        client = docker.from_env()
        client.containers.list()
        print("[ OK ]")
    except requests.exceptions.ConnectionError:
        print("[ FAILED ]")

        logging.error(
            "Either Docker is not running or you do not have the required permissions to run Docker commands. \n\n"
            "Try adding your user to the `docker` group (e.g. `sudo usermod -aG docker $USER`) and then restarting your session."
        )

        sys.exit(1)


def _start_docker_container(image, working_dir):
    client = docker.from_env()

    toolchains_folder = _find_shipped_path("toolchains")

    container = client.containers.run(
        image,
        "/bin/bash --login",
        detach=True,
        volumes={
            _find_project_root(): {
                "bind": _find_project_root(),
                "mode": "Z"
            },
            toolchains_folder: {
                "bind": toolchains_folder,
                "mode": "Z"
            }
        },
        tty=True,
        stdin_open=True,
        user=os.getuid()
    )

    return container


class Platform(object):
    required_docker_image = None
    required_toolchain_file = None
    requires_package_before_run = False
    cmake_binary = "cmake"
    executable_ext = ""

    def __init__(self, target, is_native):
        self.target = target
        self.is_native = is_native
        self.container = None

    def _run_command(self, cmd, cwd, use_docker_image=True, check_output=True):
        if use_docker_image and self.required_docker_image:
            assert(self.container)

            api_client = self.container.client.api
            exec_handle = api_client.exec_create(
                self.container.name,
                '/bin/bash -c "source /etc/bash.bashrc; cd %s; %s"' % (cwd, " ".join(cmd))
            )

            stream = api_client.exec_start(exec_handle, stream=True)

            output = ""
            for line in stream:
                print(line.decode("utf-8"), end="")
                output += line.decode("utf-8")

            exit_code = api_client.exec_inspect(
                exec_handle['Id']
            ).get('ExitCode')

            if exit_code:
                if check_output:
                    raise IOError(output)
                else:
                    return exit_code
            else:
                if check_output:
                    return output
                else:
                    return exit_code

        else:
            if check_output:
                output = subprocess.check_output(cmd, cwd=cwd)
                return output.decode("utf-8")
            else:
                return subprocess.call(
                    cmd, cwd=cwd
                )

    def _generate_cmake_args(self, release=False):
        libraries = _project_path("libraries")

        args = []

        if release:
            args.append("-DCMAKE_BUILD_TYPE=Release")
        else:
            args.append("-DCMAKE_BUILD_TYPE=Debug")

        if self.required_toolchain_file:
            args.append("-DCMAKE_TOOLCHAIN_FILE={0}".format(
                _locate_shipped_file(self.required_toolchain_file)
            ))

        args.append(
            "-DSIMULANT_INCLUDE_FOLDER={}".format(
                join(libraries, self.target, "include")
            )
        )

        if release:
            args.append(
                "-DSIMULANT_LIBRARY_FOLDER={}".format(
                    join(libraries, self.target, "lib")
                )
            )
        else:
            # Use debug library in non-release builds
            args.append(
                "-DSIMULANT_LIBRARY_FOLDER={}".format(
                    join(libraries, self.target, "lib/debug")
                )
            )

        args.append("../../..")

        return args

    def _calc_build_dir(self, release=False):
        return os.path.abspath(
            "build/{target}/{build_type}".format(
                target=self.target,
                build_type="release" if release else "debug"
            )
        )

    def check_supported(self, this_platform):
        raise NotImplementedError()

    def _prepare(self, release=False):
        if self.required_docker_image:
            _check_docker_running()
            _download_docker_image(self.required_docker_image)
            self.container = _start_docker_container(
                self.required_docker_image,
                self._calc_build_dir(release=release)
            )

    def build(self, release=False, rebuild=False, print_output=True):
        self._prepare(release=release)

        if rebuild:
            build_dir = self._calc_build_dir(release=release)
            if os.path.exists(build_dir):
                shutil.rmtree(build_dir)

        # Default behaviour is too use cmake to build
        build_dir = self._calc_build_dir(release=release)

        # Make the target directory
        if not os.path.exists(build_dir):
            os.makedirs(build_dir)

        cmake_args = self._generate_cmake_args(release=release)

        cmake_cmd = [self.cmake_binary] + cmake_args

        ret = self._run_command(cmake_cmd, cwd=build_dir, check_output=False)
        if ret:
            sys.exit(ret)

        ret = self._run_command(["make"], cwd=build_dir, check_output=False)
        if ret:
            sys.exit(ret)

    def run(self, rebuild=False, release=False):
        if rebuild:
            self.build()

        build_dir = self._calc_build_dir(release=release)

        config = _project_path("simulant.json")
        libraries = _project_path(os.path.join("libraries", self.target, "lib"))

        with open(config) as f:
            data = json.loads(f.read())
            executable = data["executable"] + self.executable_ext

        env = dict(os.environ)
        env["LD_LIBRARY_PATH"] = libraries

        subprocess.call(
            [os.path.abspath(os.path.join(build_dir, executable))],
            cwd=".",
            env=env
        )

    def package(self):
        raise NotImplementedError()

    def test(self, rebuild=True):
        if rebuild:
            self.build(release=False)

        build_dir = self._calc_build_dir(release=False)
        test_bin = join(build_dir, "tests")

        ret = self._run_command(
            [test_bin],
            cwd=_project_path("."),
            check_output=False
        )

        if ret:
            sys.exit(ret)


class Linux(Platform):
    def _prepare(self, **kwargs):
        super(Linux, self)._prepare()

        if not self.is_native:
            raise UnsupportedPlatformError(
                "linux-x86-gcc"
            )

    def package(self):
        # Install flatpak sdk and runtime
        self._run_command(
            "flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo".split(),
            cwd="."
        )

        self._run_command(
            "flatpak install -y flathub org.freedesktop.Platform//19.08 org.freedesktop.Sdk//19.08".split(),
            cwd="."
        )

        package_dir = join(_project_path("packages"), self.target)
        package_tmp_dir = join(_project_path("packages"), self.target + "-tmp")
        package_build_dir = join(package_tmp_dir, "build")

        with open(_project_path("simulant.json")) as f:
            config = json.load(f)

        package_name = config["package"].lower()

        for folder in (package_dir, package_tmp_dir):
            if os.path.exists(folder):
                shutil.rmtree(folder)

            os.makedirs(folder)

        def _generate_appdata_xml():
            appdata_file = os.path.join(package_tmp_dir, package_name + ".appdata.xml")

            def set_text(node, text):
                t = doc.createTextNode(text)
                node.appendChild(t)
                return node

            doc = Document()
            root = doc.createElement("component")
            root.setAttribute("type", "desktop")
            root.appendChild(set_text(doc.createElement("id"), package_name))
            root.appendChild(set_text(doc.createElement("name"), config["name"]))
            root.appendChild(
                set_text(
                    doc.createElement("description"), "<p>" + config["description"] + "</p>"
                )
            )

            # This is the license of the metadata file, NOT the license of the project
            root.appendChild(set_text(doc.createElement("metadata_license"), "FSFAP"))

            root.appendChild(set_text(doc.createElement("project_license"), config["license"]))

            doc.appendChild(root)

            # Look for a meta/screenshots folder and use that
            # for the appdata if found
            screenshot_dir = _project_path("meta/screenshots")
            if exists(screenshot_dir):
                screenshots = root.createElement("screenshots")
                for f in os.listdir(screenshot_dir):
                    shot = screenshots.createElement(
                        "screenshot"
                    )
                    shot.setAttribute("type", "default")
                    set_text(shot, join("/share/%s/screenshots" % package_name, f))

            with open(appdata_file, "w") as f:
                doc.writexml(f, indent="  ", addindent="  ", newl="\n")

            return appdata_file

        def _generate_manifest():
            manifest_file = os.path.join(package_tmp_dir, package_name + ".json")

            libraries_dir = join(
                _project_path("libraries"),
                self.target,
                "lib"
            )

            appdata_xml = _generate_appdata_xml()
            appdata_filename = os.path.split(appdata_xml)[1]

            data = {
                "app-id": package_name,
                "runtime": "org.freedesktop.Platform",
                "runtime-version": "19.08",
                "sdk": "org.freedesktop.Sdk",
                "command": config["executable"],
                "modules": [{
                    "name": "libraries",
                    "buildsystem": "simple",
                    "build-commands": [
                        "mkdir -p /app/lib && install libraries/*.so* /app/lib",
                    ],
                    "sources": [
                        {"type": "dir", "path": libraries_dir, "dest": "libraries"},
                    ],
                    "cleanup": [
                        "libraries",
                    ]
                }, {
                    "name": "appdata",
                    "buildsystem": "simple",
                    "build-commands": [
                        "mkdir -p /app/share/metainfo",
                        "mv {name} /app/share/metainfo/{name}".format(name=appdata_filename),
                    ],
                    "sources": [
                        {"type": "file", "path": appdata_xml}
                    ]
                }, {
                    "name": config["name"],
                    "buildsystem": "cmake",
                    "config-opts": [
                        "-DCMAKE_BUILD_TYPE=Release",
                        "-DSIMULANT_INCLUDE_FOLDER={}".format(
                            join("/run/build/%s/libraries/" % config["name"], self.target, "include")
                        ),
                        "-DSIMULANT_LIBRARY_FOLDER=/app/lib",
                    ],
                    "sources": [
                        {"type": "dir", "path": _project_path("assets"), "dest": "assets"},
                        {"type": "dir", "path": _project_path("tools"), "dest": "tools"},
                        {"type": "dir", "path": _project_path("libraries"), "dest": "libraries"},
                        {"type": "dir", "path": _project_path("sources"), "dest": "sources"},
                        {"type": "file", "path": _project_path("CMakeLists.txt")}
                    ],
                    "cleanup": [
                        "assets", "tools", "libraries", "sources", "CMakeLists.txt"
                    ]
                }, {
                    "name": "assets",
                    "buildsystem": "simple",
                    "build-commands": [
                        "cp -r assets /app/bin/assets",
                    ],
                    "sources": [
                        {"type": "dir", "path": _project_path("assets"), "dest": "assets"},
                    ],
                    "cleanup": [
                        "assets"
                    ]
                }],
                "finish-args": [
                   "--socket=wayland",
                   "--socket=x11",
                   "--share=network",
                   "--device=dri",
                   "--socket=pulseaudio"
                ],
            }

            with open(manifest_file, "w") as f:
                f.write(json.dumps(data))

            return manifest_file

        manifest_file = _generate_manifest()

        repo_dir = "%s/repo" % package_tmp_dir

        self._run_command(
            [
                "flatpak-builder", "-v",
                "--delete-build-dirs",
                "--repo=%s" % repo_dir,
                "--force-clean", package_build_dir, manifest_file
            ],
            cwd=_project_path(".")
        )

        self._run_command(
            [
                "flatpak", "build-bundle", repo_dir,
                "%s/%s" % (package_dir, config['executable'] + ".flatpak"),
                package_name
            ],
            cwd=_project_path(".")
        )

        return 0


class Windows(Platform):
    required_docker_image = "kazade/windows-sdk"
    cmake_binary = "mingw64-cmake"
    executable_ext = ".exe"

    def build(self, release=False, rebuild=False, print_output=True):
        super().build(release=release, rebuild=rebuild, print_output=print_output)

        # Copy DLLs to the build directory
        build_dir = self._calc_build_dir(release=release)

        libraries_dir = join(
            _project_path("libraries"),
            self.target,
            "lib"
        )

        if not release:
            libraries_dir = join(libraries_dir, "debug")

        for f in os.listdir(libraries_dir):
            if f.endswith(".dll"):
                target = join(build_dir, f)
                source = join(libraries_dir, f)
                shutil.copy(source, target)

    def run(self, rebuild=False, release=False):
        if rebuild:
            self.build()

        build_dir = self._calc_build_dir(release=release)

        config = _project_path("simulant.json")
        libraries = _project_path(os.path.join("libraries", self.target, "lib"))

        with open(config) as f:
            data = json.loads(f.read())
            executable = data["executable"] + self.executable_ext

        env = dict(os.environ)
        env["LD_LIBRARY_PATH"] = libraries

        subprocess.call(
            ["wine", os.path.abspath(os.path.join(build_dir, executable))],
            cwd=".",
            env=env
        )

    def test(self, rebuild=True):
        if rebuild:
            self.build(release=False)

        build_dir = self._calc_build_dir(release=False)
        test_bin = join(build_dir, "tests")

        ret = self._run_command(
            ["wine", test_bin + self.executable_ext],
            cwd=_project_path("."),
            check_output=False,
            use_docker_image=False
        )

        if ret:
            sys.exit(ret)


class Dreamcast(Platform):
    required_docker_image = "kazade/dreamcast-sdk"

    def _generate_cmake_args(self, release=False):
        args = super()._generate_cmake_args(release=release)
        args, root = args[:-1], args[-1:]

        toolchain = _find_shipped_path("toolchains/Dreamcast.cmake")
        args += ["-DCMAKE_TOOLCHAIN_FILE=%s" % toolchain]

        return args + root

    def _scramble_binary(self, cwd, binary, dest):
        bin_file = splitext(binary)[0] + ".bin"
        commands = [
            "source /etc/bash.bashrc; sh-elf-objcopy -R .stack -O binary -S -g {} {}".format(binary, bin_file),
            "source /etc/bash.bashrc; /opt/toolchains/dc/kos/utils/scramble/scramble {} {}".format(bin_file, dest),
            "rm {}".format(bin_file)  # Remove intermediate file
        ]

        print("Scrambling binary".ljust(_STR_LENGTH), end='')
        for command in commands:
            self._run_command(command.split(), cwd=cwd)
        print("[ OK ]")

    def _generate_ip_bin(self, cwd, template, text, dest):
        command = "IP_TEMPLATE_FILE={template} /opt/toolchains/dc/kos/utils/makeip/makeip {text} {dest}".format(
            template=template, dest=dest, text=text
        ).split()

        print("Generating IP.BIN".ljust(_STR_LENGTH), end='')

        self._run_command(command, cwd=cwd)

        print("[ OK ]")

        return dest

    def package(self):

        IP_TXT_TEMPLATE = """
Hardware ID   : SEGA SEGAKATANA
Maker ID      : SEGA ENTERPRISES
Device Info   : 0000 CD-ROM1/1
Area Symbols  : JUE
Peripherals   : E000F10
Product No    : T0000
Version       : V1.000
Release Date  : 20000627
Boot Filename : 1ST_READ.BIN
SW Maker Name : {author}
Game Title    : {name}
""".strip()

        # Perform a release rebuild
        self.build(release=True, rebuild=True)

        build_dir = self._calc_build_dir(release=True)
        package_dir = os.path.join(_project_path("packages"), self.target)

        if exists(package_dir):
            shutil.rmtree(package_dir)

        os.makedirs(package_dir)

        config = _project_path("simulant.json")

        with open(config) as f:
            data = json.loads(f.read())
            executable = data["executable"]

        binary = "%s.elf" % executable
        binary = join(build_dir, binary)

        # Copy assets to the output directory
        shutil.copytree("assets", join(package_dir, "assets"))

        template = _find_shipped_path("toolchains/IP.TMPL")

        # We need to copy the template into the container so that it
        # can be found.
        shutil.copy(template, join(package_dir, "IP.TMPL"))

        template = join(package_dir, "IP.TMPL")
        text = join(package_dir, "ip.txt")

        with open("simulant.json", "r") as fin:
            data = json.loads(fin.read())

        with open(text, "w") as fout:
            fout.write(IP_TXT_TEMPLATE.format(**data))

        ip_bin = self._generate_ip_bin(
            package_dir,
            template,
            text,
            join(package_dir, "IP.BIN")
        )

        # Remove the IP.TMPL we created
        os.remove(template)

        # Don't need the text file any more
        os.remove(text)

        self._scramble_binary(
            package_dir,
            binary,
            join(package_dir, "1ST_READ.BIN")
        )

        print("Building ISO".ljust(_STR_LENGTH), end='')
        self._run_command(
            "mkisofs -C 0,11702 -V {title} -G {ip_bin} -joliet -rock -l -o {name}.iso {package_dir}".format(
                title=data["name"].replace(" ", r"\ "),
                ip_bin=ip_bin,
                name=data["executable"],
                package_dir=package_dir
            ).split(),
            cwd=package_dir
        )
        print("[ OK ]")

        print("Generating CDI".ljust(_STR_LENGTH), end='')
        self._run_command(
            "/opt/toolchains/dc/kos/utils/img4dc/build/cdi4dc/cdi4dc {name}.iso {name}.cdi".format(
                name=data["executable"]
            ).split(),
            cwd=package_dir
        )
        print("[ OK ]")

        # return os.path.join(package_dir, "%s.cdi" % data["executable"])
        return 0

    def run(self, rebuild=True, release=True):
        supported_emulators = ("redream", "lxdream")
        commands = {
            "lxdream": "lxdream -b",
            "redream": "redream"
        }

        package_dir = os.path.join(_project_path("packages"), self.target)

        with open("simulant.json", "r") as fin:
            data = json.loads(fin.read())

        cdi_path = os.path.join(package_dir, data["executable"] + ".cdi")

        if not os.path.exists(cdi_path) or rebuild:
            # We need to package things up to run the dreamcast build
            ret_code = self.package()
            assert(ret_code == 0)

        for emu in supported_emulators:
            try:
                self._run_command(
                    ("%s --help" % emu).split(),
                    cwd=os.path.dirname(cdi_path),
                    use_docker_image=False
                )
                break
            except (subprocess.CalledProcessError, FileNotFoundError,):
                print("Unable to find %s" % emu)
                continue
        else:
            print(
                "Unable to find a supported emulator. Install one of %s" % supported_emulators
            )
            sys.exit(1)

        self._run_command(
            ("%s %s" % (commands[emu], cdi_path)).split(),
            cwd=os.path.dirname(cdi_path),
            use_docker_image=False
        )


class Android(Platform):
    required_docker_image = "kazade/android-sdk"
    pass


def update(update_libraries=True, **kwargs):
    print("Downloading assets")

    # Make the assets dir if it doesn't exist
    assets_dir = _project_path("assets")
    if not os.path.exists(assets_dir):
        os.makedirs(assets_dir)

    # Make a temporary directory for the download
    assets_dir_tmp = _project_path(".assets")
    if not os.path.exists(assets_dir_tmp):
        os.makedirs(assets_dir_tmp)

    # Download and extract
    _download_zip_and_extract(
        _DOWNLOAD_ROOT + "/master/assets.zip",
        assets_dir_tmp
    )

    # Overwrite the simulant dir in the assets folder
    if os.path.exists(join(assets_dir, "simulant")):
        shutil.rmtree(join(assets_dir, "simulant"))
    shutil.move(join(assets_dir_tmp, "simulant"), assets_dir)

    # Remove the temp folder
    shutil.rmtree(assets_dir_tmp)

    print("Downloading tools")
    tools_dir = _project_path("tools/simulant")
    if os.path.exists(tools_dir):
        shutil.rmtree(tools_dir)

    os.makedirs(tools_dir)

    _download_zip_and_extract(
        _DOWNLOAD_ROOT + "/master/tools.zip",
        os.path.dirname(tools_dir)
    )

    # Make the test generate executable
    _make_executable(os.path.join(tools_dir, "test_generator.py"))

    if not update_libraries:
        return

    targets = ("linux-x64-gcc", "windows-x64-mingw", "dreamcast-sh4-gcc")

    lib_dir = _project_path("libraries")
    if not os.path.exists(lib_dir):
        os.makedirs(lib_dir)

    for target in targets:
        print("Downloading files for %s".ljust(_STR_LENGTH) % target)

        target_path = os.path.join(lib_dir, target)
        if os.path.exists(target_path):
            shutil.rmtree(target_path)

        _download_zip_and_extract(
            _DOWNLOAD_ROOT + "/master/{target}.zip".format(
                target=target
            ), lib_dir
        )


def start(name, target_folder, force=False):
    def _sanitize_name(name):
        return name.replace("/", "_")

    name = _sanitize_name(name)

    template_folder = _find_shipped_path("template")

    output_folder = os.path.join(target_folder, name)

    if os.path.exists(output_folder) and not force:
        result = input("The target folder already exists and will be replaced, this cannot be undone. Continue? [y/N] ")
        if result.lower() != "y":
            return 0
        else:
            # Remove the directory before replacing it
            shutil.rmtree(output_folder)

    shutil.copytree(template_folder, output_folder)

    # Rename any files that have the project name in them
    _rename_files_and_directories(
        "__project_name__",
        name.replace(" ", "_").lower(),
        output_folder
    )

    # Replace the project name throughout
    _search_replace("__project_name__", name, output_folder)

    # These two replace with project_name or PROJECT_NAME respectively
    _search_replace("__project_name_lower__", name.lower().replace(" ", "_"), output_folder)
    _search_replace("__project_name_upper__", name.upper().replace(" ", "_"), output_folder)

    pascal_name = name.replace("-", " ").title().replace(" ", "")

    # Replace with ProjectName
    _search_replace("__project_name_pascal__", pascal_name, output_folder)

    os.chdir(output_folder)
    update()


_SUPPORTED_TARGETS = [
    "linux",
    "windows",
    "dreamcast",
    "android"
]


def _build_args():
    parser = argparse.ArgumentParser(description="Simulant Tool")
    subs = parser.add_subparsers(dest="subcommand")

    build = subs.add_parser("build", help="Build your project")
    build.add_argument("platform", default="native", nargs="?",
        choices=["native"] + _SUPPORTED_TARGETS
    )
    build.add_argument("--rebuild", default=False, action="store_true")
    build.add_argument("--release", default=False, action="store_true")
    build.add_argument(
        "--use-global-simulant", default=False, action="store_true",
        help="When specified, search the system for the Simulant includes and libraries"
    )

    package = subs.add_parser("package", help="Package your project for a particular platform")
    package.add_argument("platform", default="native", nargs="?",
        choices=["native"] + _SUPPORTED_TARGETS
    )

    start = subs.add_parser("start", help="Start a new Simulant project from a template")
    start.add_argument("project", action="store", type=str)
    start.add_argument("target", default=".", nargs="?", type=str)
    start.add_argument("-f", dest="force", action="store_true")

    test = subs.add_parser("test", help="Test your Simulant project")
    test.add_argument("platform", default="native", nargs="?",
        choices=["native"] + _SUPPORTED_TARGETS
    )

    run = subs.add_parser("run", help="Run your application")
    run.add_argument("platform", default="native", nargs="?",
        choices=["native"] + _SUPPORTED_TARGETS
    )
    run.add_argument("--rebuild", default=False, action="store_true")
    run.add_argument("--release", default=False, action="store_true")

    update = subs.add_parser("update", help="Update Simulant library")
    update.add_argument("--assets-only", default=False, action="store_true")

    opts = parser.parse_args()

    return opts


def _normalize_target(target):
    if target == "native":
        return _native_target()

    if target == "windows":
        return "windows-x64-mingw"

    if target == "linux":
        return "linux-x64-gcc"

    if target == "dreamcast":
        return "dreamcast-sh4-gcc"

    raise UnsupportedPlatformError(target)


def _native_target():
    if platform.system() == 'Linux':
        if platform.machine() == 'x86_64':
            return 'linux-x64-gcc'
    elif platform.system() == 'Darwin':
        return 'darwin-x64-gcc'

    raise UnsupportedPlatformError(platform.system())


_PLATFORMS = {
    "linux-x64-gcc": Linux,
    "windows-x64-mingw": Windows,
    "dreamcast-sh4-gcc": Dreamcast,
}


def main():
    opts = _build_args()

    # Start and update aren't platform specific
    if opts.subcommand == "start":
        return start(opts.project, opts.target, force=opts.force)
    elif opts.subcommand == "update":
        return update(
            update_libraries=(not opts.assets_only)
        )

    try:
        this_target = _native_target()
        target = _normalize_target(opts.platform)

        platform = _PLATFORMS[target](
            target,
            is_native=this_target == target
        )

        if opts.subcommand == "build":
            return platform.build(
                release=opts.release,
                rebuild=opts.rebuild
            )
        elif opts.subcommand == "run":
            return platform.run(
                rebuild=opts.rebuild,
                release=opts.release
            )
        elif opts.subcommand == "test":
            return platform.test()
        elif opts.subcommand == "package":
            return platform.package()
        elif opts.subcommand == "debug":
            return platform.debug()

    except UnsupportedPlatformError as e:
        print(e.message)
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
